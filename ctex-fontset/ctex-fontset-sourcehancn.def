%%
%% This is file `ctex-fontset-sourcehancn.def',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% ctex.dtx  (with options: `fontset,sourcehancn')
%% 
%%     Copyright (C) 2003--2018, 2022
%%     CTEX.ORG and any individual authors listed in the documentation.
%% ------------------------------------------------------------------------------
%% 
%%     This work may be distributed and/or modified under the
%%     conditions of the LaTeX Project Public License, either
%%     version 1.3c of this license or (at your option) any later
%%     version. This version of this license is in
%%        http://www.latex-project.org/lppl/lppl-1-3c.txt
%%     and the latest version of this license is in
%%        http://www.latex-project.org/lppl.txt
%%     and version 1.3 or later is part of all distributions of
%%     LaTeX version 2005/12/01 or later.
%% 
%%     This work has the LPPL maintenance status `maintained'.
%% 
%%     The Current Maintainers of this work are Leo Liu, Qing Lee and Liam Huang.
%% 
%% ------------------------------------------------------------------------------
%% 
\GetIdInfo$Id: ctex.dtx 096f319 2018-05-02 19:51:41 +0800 Liam Huang <liamhuang0205@gmail.com> $
  {Ubuntu fonts definition (CTEX)}
\ProvidesExplFile{ctex-fontset-sourcehancn.def}
  {\ExplFileDate}{2.4.14}{\ExplFileDescription}
\sys_if_engine_pdftex:TF
  {
    \ctex_zhmap_case:nnn
      {
        \setCJKmainfont
          [ BoldFont = Source~Han~Serif~CN , ItalicFont = AR~PL~UKai~CN ] { Source~Han~Sans~CN }
        \setCJKsansfont { Source~Han~Serif~CN }
        \setCJKmonofont { Source~Han~Sans~CN }
        \setCJKfamilyfont { zhsong } { Source~Han~Sans~CN }
        \setCJKfamilyfont { zhhei }  { Source~Han~Serif~CN }
        \setCJKfamilyfont { zhkai }  { AR~PL~UKai~CN }
        \ctex_punct_set:n { ubuntu }
        \ctex_punct_map_family:nn { \CJKrmdefault } { zhsong }
        \ctex_punct_map_family:nn { \CJKsfdefault } { zhhei }
        \ctex_punct_map_family:nn { \CJKttdefault } { zhsong }
        \ctex_punct_map_itshape:nn { \CJKrmdefault } { zhkai }
        \ctex_punct_map_bfseries:nn { \CJKrmdefault } { zhhei }
      }
      {
        \ctex_load_zhmap:nnnn { rm } { zhhei } { zhsong } { zhubuntufonts }
        \ctex_punct_set:n { notocjksc }
        \ctex_punct_map_family:nn { \CJKrmdefault } { zhsong }
        \ctex_punct_map_bfseries:nn { \CJKrmdefault } { zhhei }
        \ctex_punct_map_itshape:nn { \CJKrmdefault } { zhkai }
      }
      { \ctex_fontset_error:n { sourcehancn } }
  }
  {
    \sys_if_engine_uptex:TF
      {
        \ctex_set_upfonts:nnnnnn
          { Source~Han~Sans~CN } { Source~Han~Serif~CN } { AR~PL~UKai~CN }
          { Source~Han~Serif~CN } { Source~Han~Serif~CN }
          { Source~Han~Sans~CN }
        \ctex_set_upfamily:nnn { zhsong } { upzhserif } {}
        \ctex_set_upfamily:nnn { zhhei } { upzhsans } {}
        \ctex_set_upfamily:nnn { zhkai } { upzhserifit } {}
      }
      {
        \setCJKmainfont
          [ BoldFont = Source~Han~Sans~CN , ItalicFont = AR~PL~UKai~CN ] { Source~Han~Serif~CN }
        \setCJKsansfont { Source~Han~Sans~CN }
        \setCJKmonofont { Source~Han~Serif~CN }
        \setCJKfamilyfont { zhsong } { Source~Han~Serif~CN }
        \setCJKfamilyfont { zhhei }  { Source~Han~Sans~CN }
        \setCJKfamilyfont { zhkai }  { AR~PL~UKai~CN }
      }
  }
\NewDocumentCommand \songti   { } { \CJKfamily { zhsong } }
\NewDocumentCommand \heiti    { } { \CJKfamily { zhhei } }
\NewDocumentCommand \kaishu   { } { \CJKfamily { zhkai } }
%% 
%%
%% End of file `ctex-fontset-sourcehancn.def'.
