# texlive-ctex-fontset-uos

  Gitee: https://gitee.com/careone/ctex-fontset-uos

  * 2022-01-21, v2.4.14-1, Careone 整理

  * 2022-01-22, v2.4.14-2, Careone 更新

## 简介

LaTeX CTeX plugins to use Chinese fonts "CESI 18030", "Noto CJK SC" and more.

 以插件形式实现LaTeX排版软件的CTeX中文宏集对CESI中文字体, Noto中文字体, 以及思源宋体/
 黑体 CN 的支持。内含3套中文常用字体（主要适用于中国大陆字形风格）的支持：

  * CESI (中国电子) 相关中文字体 (包括：CESI 18030 书宋/黑体/仿宋/楷体/小标宋，共5款);
  * Noto Serif/Sans CJK SC (中国大陆简体中文字体);
  * 思源宋体 CN (Source Han Serif CN), 思源黑体 CN (Source Han Sans CN)。

 特别提示：相关 *.def 字体配置文件，暂未经过严格验证。请自行评估使用风险！

 如果代码有缺陷或优化方案，欢迎指正、指点！

#### 技术说明

  操作步骤：

1. 把3个 def 文件，复制到指定目录。
   
   Linux 操作系统下，一般放在目录 /usr/share/texlive/texmf-dist/tex/latex/ctex/fontset/ 下面。

   相关 def 配置文件对应的 DEB 软件包名：texlive-lang-chinese

2. 以 root 用户运行命令 mktexlsr 更新 LaTeX 宏包/文类/资源索引数据库。

3. 在 tex 源码中，以文类引用，或者宏包引用的方式，添加可选参数

 [fontset=cesi]
 
  或者
  [fontset=notocjksc]
  
  或者
  [fontset=sourcehancn]

  即可生效。

（注：使用前，请确保当前系统中，已安装相应的中文字体！）


#### 代码用法示例

  * 用法示例1 (适用于 UOS/Deepin Linux，或者安装了 CESI 18030 相关中文字体的用户)

 \documentclass[fontset=cesi]{ctexart}

 %\usepackage[fontset=cesi]{ctex}
 
  * 用法示例2 (适用于安装了 Noto Serif/Sans CJK SC 相关中文字体的用户):

 \documentclass[fontset=notocjksc]{ctexart}

 %\usepackage[fontset=notocjksc]{ctex}

  * 用法示例3 (适用于安装了 Source Han Serif CN 和 Source Han Sans CN 中文字体的用户):

 \documentclass[fontset=sourcehansc]{ctexart}

 %\usepackage[fontset=sourcehansc]{ctex}

#### 相关 def 字体配置文件

 usr/share/texlive/texmf-dist/tex/latex/ctex/fontset/ctex-fontset-cesi.def

 usr/share/texlive/texmf-dist/tex/latex/ctex/fontset/ctex-fontset-notocjksc.def

 usr/share/texlive/texmf-dist/tex/latex/ctex/fontset/ctex-fontset-sourcehansc.def